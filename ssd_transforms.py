import torch
from . import transforms as T

def ssd_train_transform(backbone_type: str, hflip_prob=0.5, mean=(123.0, 117.0, 104.0)):
    if backbone_type == 'vgg':
        return  T.Compose([
                T.RandomPhotometricDistort(),
                T.RandomZoomOut(fill=list(mean)),
                T.RandomIoUCrop(),
                T.RandomHorizontalFlip(p=hflip_prob),
                T.ToTensor(),
            ])
    elif backbone_type == 'mobilenet':
        return T.Compose([
                T.RandomIoUCrop(),
                T.RandomHorizontalFlip(p=hflip_prob),
                T.ToTensor(),
            ])
